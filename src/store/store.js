import Vue from "vue";
import Vuex from "vuex";
//then you use Vuex
Vue.use(Vuex);

export default new Vuex.Store({
        debug: true,
        state: {
            lastEventId: 11,
            links: [
                {"href": 'signup', "title": "Регистрация", "color": "indigo"},
                {"href": 'login', "title": "Войти", "color": "indigo"},
                {"href": 'search', "title": "Поиск конференций", "color": "green"},
            ],
            account: {
                firstName: "",
                lastName: "",
                password: "",
                email: ""
            },
            accounts: {
                "test@email.com": {
                    firstName: "Иванов",
                    lastName: "Иван",
                    password: "123123123",
                    email: "test@email.com"
                },
                "test1@email.com": {
                    firstName: "Петров",
                    lastName: "Петр",
                    password: "123123123",
                    email: "test1@email.com"
                },
                "test2@email.com": {
                    firstName: "Андреев",
                    lastName: "Андрей",
                    password: "123123123",
                    email: "test2@email.com"
                },
                "test3@email.com": {
                    firstName: "Петров",
                    lastName: "Андрей",
                    password: "123123123",
                    email: "test3@email.com"
                },
            },
            registrations: [
                {eventId: 3, moderation: "approved"},
                {eventId: 5, moderation: "approved"},
                {eventId: 10, moderation: "approved"},
            ],
            registeredEvents: {
                "test1@email.com": [{id: 1, status: 'requested'}, {id: 4, status: 'requested'}, {
                    id: 7,
                    status: 'approved'
                }, {id: 8, status: 'declined'}, {id: 10, status: 'requested'}],
                "test2@email.com": [{id: 4, status: 'requested'}, {id: 8, status: 'approved'}, {
                    id: 10,
                    status: 'approved'
                }],
                "test3@email.com": [{id: 4, status: 'requested'}, {id: 8, status: 'declined'}, {
                    id: 10,
                    status: 'approved'
                }],
                "test@email.com": [{id: 9, status: 'approved'}]
            },
            ownEvents: {
                "test@email.com": [1, 2, 3, 4, 5, 6, 7, 8, 10],
                "test1@email.com": [9, 10],
                "test2@email.com": [10]
            },
            events: [
                {
                    "id": 1,
                    "broadcast": 2,
                    "time": "2020-10-01 11:00",
                    "name": "Арт-кафе «Культурный резонанс». Познание себя через искусство.",
                    "desc": "Приглашаем всех желающих в доверительной обстановке  соприкоснуться  с собой настоящим  через  произведение искусства.\n" +
                        "Каждый раз обсуждение картины в Арт-кафе вызывает много новых впечатлений и открытий для себя и всей группы важных смыслов.\n" +
                        "Это происходит благодаря «культурному резонансу», возникающему при общении участников и групповом обсуждении выдающегося произведения искусства."
                },
                {
                    "id": 2,
                    "broadcast": 2,
                    "time": "2020-10-21 13:00",
                    "name": "VIII Всероссийский молодежный фестиваль рекламы «Спектр»",
                    "desc": "Номинации Фестиваля:\n– «Социальный проект» – рекламный видеоролик или презентация в электронном виде," +
                        " посвященные решению социальных проблем, существующих в молодежной среде;\n– «Маркетинговый проект»  –" +
                        " рекламный видеоролик или макет рекламного плаката в электронном виде, продвигающие коммерческие и некоммерческие " +
                        "предприятия и проекты, созданные или планируемые к внедрению. Участники фестиваля могут выбрать" +
                        " любое российское предприятие или проект с целью продвижения, также брифы (технические задания) " +
                        "предприятий-спонсоров по данной номинации будут размещаться в группе кафедры маркетинга и торгового дела в социальной сети «Вконтакте»"
                },
                {
                    "id": 3,
                    "broadcast": 2,
                    "time": "2020-10-02 14:00",
                    "name": "Школа-семинар для молодых исследователей «Научный стартап: лингвистика и лингводидактика»",
                    "desc": "Школа-семинар призвана объединить молодых исследователей в области методики обучения иностранным языкам, германской и восточной филологии, литературоведения, педагогики. Это площадка для верификации своей научной траектории, апробации научных идей и для дискуссии с экспертами в профильных областях.\n" +
                        "Наши эксперты готовы оценить проделанную молодыми учеными работу, а также высказать замечания и рекомендации по организации самостоятельной работы над диссертационными исследованиями, подбору научной литературы, помочь в организации экспериментальной работы."
                },
                {
                    "id": 5,
                    "broadcast": 0,
                    "time": "2020-12-26 14:00",
                    "name": "Встреча актива студенческого совета АБиБ со студентами первого курса",
                    "desc": "Актив студенческого совета АБиБ проводит встречу со студентами первого курса. Активисты расскажут о структуре студенческого совета Академии, о проектах, реализуемых студентами биологами и почвоведами, а также поведают о возможностях реализации творческих талантов студентов. Опытные спикеры расскажут также о деятельности студенческого научного общества АБиБ и о деятельности первичной профсоюзной организации обучающихся. "
                },
                {
                    "id": 6,
                    "broadcast": 0,
                    "time": "2020-12-27 15:00",
                    "name": "Деловая игра \"Открой свое дело\"",
                    "desc": "Деловая игра «Открой свое дело» ставит своей задачей разработку полноценного инвестиционного проекта и презентация данного проекта целевой аудитории, которая рассматривается в деловой игре в качестве «потенциальных инвесторов».  В ходе мероприятия будут представлены четыре инвестиционных проекта для различных сфер бизнеса,  обсуждение представленных проектов и выбор лучшего проекта, путем голосования.\n"
                },
                {
                    "id": 4,
                    "broadcast": 0,
                    "time": "2021-01-09 14:30",
                    "name": "Молодежный форум дизайна и развития городских систем «Новогодняя Столица»",
                    "desc": "В форуме примут участие более 100 учащихся образовательных организаций и лидеров молодежных объединений Центрального федерального округа, а также соотечественники, проживающие за рубежом, имеющие опыт и заинтересованные в развитии привлекательности городов.\n" +
                        "Форум пройдет в рамках гранта Федерального агентства по делам молодежи для победителей Всероссийского конкурса молодежных проектов."
                },
                {
                    "id": 7,
                    "time": "2021-02-12 17:00",
                    "name": "Мастер-класс по программированию на языке Python для подростков. Модуль 3",
                    "desc": "В ходе мастер-класса участники научатся генерировать идеи, подключать и использовать модули стандартной библиотеки, работать с документацией в стандартной библиотеке, работать с внешними библиотеками и утилитой pip, объективно оценивать результаты своей работы."
                },
                {
                    "id": 8,
                    "broadcast": 0,
                    "time": "2021-01-15 18:00",
                    "name": "Студвесна 2.0",
                    "desc": "Собрание фокус-группы студенческого фестиваля «Студенческая весна-2020»"
                },
                {
                    "id": 9,
                    "broadcast": 0,
                    "time": "2021-02-25 19:00",
                    "name": "Студенческий фестиваль выходного дня \"Студ ART\"",
                    "desc": "Презентация студенческих объединений для привлечения первокурсников к активистской деятельности"
                },
                {
                    "id": 10,
                    "broadcast": 0,
                    "time": "2021-02-30 20:00",
                    "name": "Цепная реакция: открытые студенческие лекции о науке",
                    "desc": "В университете есть студенты, которые проводят научные исследования в студенческих конструкторских бюро и научно-исследовательских лабораториях. В формате общественных слушаний расскажут об области своих исследований и попытаются ответить на вопрос: в какой предметной области находится их исследование, почему это интересно и кому это нужно?\n" +
                        "Выступающие расскажут о том, как они пришли в науку, о своих научных достижениях, ответят на вопрос, чем вообще интересна его сфера исследований, какие тренды сегодня нужно учитывать и почему заниматься наукой важно и интересно."
                },
            ],
            recordLastId: 3,
            records: {
                "test@email.com": [
                    {
                        "id": 1,
                        "timeStart": "2020-09-01 11:00:00",
                        "timeEnd": "2020-09-01 11:33:01",
                        "name": "«Культурный резонанс». Первый этап.",
                    },
                    {
                        "id": 2,
                        "timeStart": "2020-08-21 13:00:11",
                        "timeEnd": "2020-08-21 14:00::33",
                        "name": "VII Всероссийский молодежный фестиваль рекламы",
                    },
                ]
            },
            messages: {}
        },
        mutations: {
            initialiseStore(state) {
                if (localStorage.getItem('store')) {
                    let data = JSON.parse(localStorage.getItem('store'));
                    this.replaceState(
                        Object.assign(state, data)
                    );
                }
            },
            setAccount(state, data) {
                state.account.firstName = data.firstName;
                state.account.lastName = data.lastName;
                state.account.email = data.email;
                state.account.password = data.password;
                state.accounts[data.email] = data;
            },
            resetAccount(state) {
                state.account = {
                    firstName: "",
                    lastName: "",
                    password: "",
                    email: ""
                }
            },
            eventRegisterToggle(state, id) {
                let events = state.registeredEvents[state.account.email];
                if (events === undefined) {
                    events = []
                }
                let index = -1;
                events.forEach((req, ind) => {
                    if (req.id == id) {
                        index = ind;
                    }
                })

                if (index !== -1) {
                    events = events.filter((req) => {
                        return req.id != id
                    });
                } else {
                    events.push({id: id, status: "requested"});
                }

                state.registeredEvents[state.account.email] = events;
            },
            updateEvent(state, ev) {
                let event = ev.event;
                let events = state.events
                if (ev.action === 'create') {
                    events.push(event);
                    state.ownEvents[state.account.email].push(event.id)
                    state.lastEventId++;
                } else {
                    events = state.events.map((e) => {
                        if (e.id == event.id) {
                            return event;
                        }
                        return e
                    });
                }
                state.events = events
            },
            updateRegStatus(state, ev) {
                let regs = state.registeredEvents;
                Object.entries(regs)
                    .forEach((acc) => {
                        if (acc[0] != ev.email) {
                            return
                        }
                        regs[acc[0]] = acc[1].map((req) => {
                            if (ev.eventId == req.id) {
                                req.status = ev.status
                            }
                            return req
                        })
                    })
                state.registeredEvents = regs
            },
            deleteEvent(state, id) {
                let events = state.events;
                events = events.filter((e) => {
                    return e.id != id
                })
                state.events = events;
            },
            startEvent(state, id) {
                let events = state.events;
                events = events.map((e) => {
                    if (e.id == id) {
                        e.broadcast = 1
                    }
                    return e
                })
                state.events = events;
            },
            sendMessage(state, message) {
                let messages = state.messages;
                if (!(message.eventId in messages)) {
                    messages[message.eventId] = []
                }
                messages[message.eventId].push(message)
                state.messages = messages;
            },
            createRecord(state, record) {
                record.id = state.recordLastId
                let records = [];
                if (state.account.email in state.records) {
                    records = state.records[state.account.email]
                }
                records.push(record);
                state.records[state.account.email] = records
                state.recordLastId = state.recordLastId + 1
            },
            stopEventBroadcast(state, ev) {
                let events = state.events;
                events = events.map((el) => {
                    if (el.id == ev.id) {
                        el.broadcast = false;
                    }
                    return el;
                })
                state.events = events;
            }
        },
        getters: {
            findAccount: (state) => (email, password) => {
                let account = state.accounts[email]
                if (account && account.password === password) {
                    return account;
                }
                return null;
            },
            findEmail: (state) => (email) => {
                let account = state.accounts[email]
                if (account === undefined) {
                    return null;
                }
                return account;
            },
            findEvent: (state) => (id) => {
                return state.events.find((el) => {
                    return el.id == id;
                })
            },
            findEventOrganizer: (state) => (id) => {
                let owner = '';
                Object.entries(state.ownEvents).forEach((el) => {
                    el[1].forEach((eid) => {
                        if (eid == id) {
                            owner = state.accounts[el[0]].firstName + ' ' + state.accounts[el[0]].lastName;
                        }
                    })
                });
                return owner
            },
            registered: (state) => (id) => {
                if (state.registeredEvents[state.account.email] === undefined) {
                    return false;
                }

                let registered = false;
                state.registeredEvents[state.account.email].forEach((req) => {
                    if (req.id == id) {
                        registered = req.id == id;
                    }
                })

                return registered;
            },
            registrationStatus: (state) => (id) => {
                if (state.registeredEvents[state.account.email] === undefined) {
                    return false;
                }

                let status = false;
                state.registeredEvents[state.account.email].forEach((req) => {
                    if (req.id == id) {
                        status = req.status;
                    }
                })

                return status;
            },
            findUserRegistrations: (state) => () => {
                return state.registeredEvents[state.account.email]
            },
            accountEventRegStatus: (state) => (email, eventId) => {
                let status = '';
                if (state.registeredEvents[email] === undefined) {
                    return status;
                }
                state.registeredEvents[email].forEach((req) => {
                    if (req.id == eventId) {
                        status = req.status;
                    }
                })
                return status;
            },
            findUserOwnEvents: (state) => () => {
                return state.ownEvents[state.account.email]
            },
            links: state => {
                if (state.account.email) {
                    return [
                        {
                            "href": '/', "title": "Выйти", "color": "indigo", "onclick": function (store, router) {
                                store.commit('resetAccount');
                                router.push('/')
                            }
                        },
                        {"href": '/profile', "title": "Профиль", "color": "indigo"},
                        {"href": '/manage', "title": "Мои конференции", "color": "green"},
                        {"href": '/search', "title": "Поиск конференций", "color": "green"},
                    ]
                }

                return [
                    {"href": '/signup', "title": "Регистрация", "color": "indigo"},
                    {"href": '/login', "title": "Войти", "color": "indigo"},
                    {"href": '/search', "title": "Поиск конференций", "color": "green"},
                ]
            },
            findOwner: (state) => (eventId) => {
                let account = '';
                Object.entries(state.ownEvents).forEach((ent) => {
                    ent[1].forEach((eid) => {
                        if (eventId == eid) {
                            account = state.accounts[ent[0]]
                        }
                    })
                })
                return account
            }
        }
    }
);
