import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);
import colors from 'vuetify/lib/util/colors'
import ru from 'vuetify/es5/locale/ru'

export default new Vuetify({

  lang: {
    locales: { ru },
    current: 'ru',
  },
  build: {
    extractCss: true
  },
  theme: {
    themes: {
      light: {
        primary: colors.indigo,
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
      },
    },
  },
});
