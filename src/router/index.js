import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
import store from "@/store/store"

let isAuthenticated = function () {
    return store.state.account.email !== ''
}

const routes = [
    {
        path: '/',
        name: 'Главная',
        component: () => import( '../views/Main.vue')
    },
    {
        path: '/signup',
        name: 'Регистрация',
        component: () => import( '../views/Signup.vue')
    },
    {
        path: '/login',
        name: 'Вход',
        component: () => import( '../views/Login.vue')
    },
    {
        path: '/password-reset',
        name: 'Восстановление пароля',
        component: () => import( '../views/PasswordReset.vue')
    },
    {
        path: '/search',
        name: 'Конференции',
        component: () => import( '../views/Search.vue')
    },
    {
        path: '/profile',
        name: 'Аккаунт',
        component: () => import( '../views/Profile.vue'),
        beforeEnter: (to, from, next) => {
            if (to.name !== 'Вход' && !isAuthenticated()) next({name: 'Вход'})
            else next()
        }
    },
    {
        path: '/manage',
        name: 'Управление конференциями',
        component: () => import( '../views/Manage.vue'),
        beforeEnter: (to, from, next) => {
            if (to.name !== 'Вход' && !isAuthenticated()) next({name: 'Вход'})
            else next()
        }
    },
    {
        path: '/records',
        name: 'Записи конференций',
        component: () => import( '../views/Records.vue'),
        beforeEnter: (to, from, next) => {
            if (to.name !== 'Вход' && !isAuthenticated()) next({name: 'Вход'})
            else next()
        }
    },
    {
        path: '/event/:id',
        name: 'Редактирование конференции',
        component: () => import( '../views/Event.vue')
    },
    {
        path: '/records/:id',
        name: 'Просмотр записи',
        component: () => import( '../views/Record.vue'),
        beforeEnter: (to, from, next) => {
            if (to.name !== 'Вход' && !isAuthenticated()) next({name: 'Вход'})
            else next()
        }
    },
    {
        path: '/manage/event/create',
        name: 'Создание конференции',
        component: () => import( '../views/ManageEvent.vue'),
        beforeEnter: (to, from, next) => {
            if (to.name !== 'Вход' && !isAuthenticated()) next({name: 'Вход'})
            else next()
        }
    },
    {
        path: '/manage/event/edit/:id',
        name: 'Редактирование конференции',
        component: () => import( '../views/ManageEvent.vue'),
        beforeEnter: (to, from, next) => {
            if (to.name !== 'Вход' && !isAuthenticated()) next({name: 'Вход'})
            else next()
        }
    },
    {
        path: '/broadcast/:id',
        name: 'Проведение конференции',
        component: () => import( '../views/Broadcast.vue'),
        beforeEnter: (to, from, next) => {
            if (to.name !== 'Вход' && !isAuthenticated()) next({name: 'Вход'})
            else {
                let event = store.getters.findEvent(to.params.id);
                if (!event) {
                    next('/manage')
                } else {
                    next()
                }
            }

        }
    },
    {
        path: '/participate/:id',
        name: 'Просмотр конференции',
        component: () => import( '../views/Participate.vue'),
        beforeEnter: (to, from, next) => {
            if (to.name !== 'Вход' && !isAuthenticated()) next({name: 'Вход'})
            else {
                let event = store.getters.findEvent(to.params.id);
                if (!event.broadcast) {
                    next('/manage')
                } else {
                    next()
                }
            }

        }
    },
]

const router = new VueRouter({
    routes
})

export default router
